import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  formularioLogueo: FormGroup;

  constructor(private fbuilder: FormBuilder, private auth:AuthService, private _router: Router) {
    this.crearFormulario();
  }

  ngOnInit(): void {}

  crearFormulario() {
    this.formularioLogueo = this.fbuilder.group({
      contrasena: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(15),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
        ],
      ],
    });
  }

  guardar() {
    const {email, contrasena} = this.formularioLogueo.value;
    this.auth.login(email,contrasena).subscribe(res =>{
      console.log(res);
      if (res)
      {
        this._router.navigateByUrl('/home')
      } else {
        console.log('ERR');
        
      }
    })
  }
}
