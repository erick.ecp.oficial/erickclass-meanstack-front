export interface Producto {
  nombre: string,
  descripcion: string,
  urlImg: string,
  precio: number,
  marca: string
}

export interface Respuesta {
  ok: boolean,
  mensaje: string
}
